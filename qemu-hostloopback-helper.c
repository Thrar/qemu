#include "qemu/osdep.h"

#include <sys/ioctl.h>
#include <net/if.h>


static void usage(void)
{
    fprintf(stderr,
            "Usage: qemu-hostloopback-helper ifname\n");
}

static void prep_ifreq(struct ifreq *ifr, const char *ifname)
{
    memset(ifr, 0, sizeof(*ifr));
    snprintf(ifr->ifr_name, IFNAMSIZ, "%s", ifname);
}

int main(int argc, char **argv)
{
    char *iface;
    int ctlfd = -1;
    struct ifreq ifr;

    if (argc < 2) {
        usage();
        return EXIT_FAILURE;
    }
    iface = argv[1];

    /* open a socket to use to control the network interfaces */
    ctlfd = socket(AF_INET, SOCK_STREAM, 0);
    if (ctlfd == -1) {
        fprintf(stderr, "failed to open control socket: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    /* bring the interface up */
    prep_ifreq(&ifr, iface);
    if (ioctl(ctlfd, SIOCGIFFLAGS, &ifr) == -1) {
        fprintf(stderr, "failed to get interface flags for `%s': %s\n",
                iface, strerror(errno));
        close(ctlfd);
        return EXIT_FAILURE;
    }

    ifr.ifr_flags |= IFF_UP;
    if (ioctl(ctlfd, SIOCSIFFLAGS, &ifr) == -1) {
        fprintf(stderr, "failed to bring up interface `%s': %s\n",
                iface, strerror(errno));
        close(ctlfd);
        return EXIT_FAILURE;
    }

    /* ... */

    /* profit! */

    return EXIT_SUCCESS;
}
